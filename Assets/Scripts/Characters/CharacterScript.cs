﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterScript : MonoBehaviour
{
    [Header("Health")]
    [SerializeField] protected float currentHealth = 100;  //текущее здоровье
    [SerializeField] protected float maxHealth = 100;      //максимальное здоровье
    [SerializeField] protected Slider healthBar;           //шкала здоровья

    void Start()
    {
        currentHealth = maxHealth;
        healthBar.maxValue = maxHealth;
        healthBar.value = currentHealth;
    }

    public void Damage(float damageAmount)   //урон
    {
        currentHealth -= damageAmount;
        healthBar.value = currentHealth;

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die() { }

    public void Healing(float healingAmount)   //лечение
    {
        currentHealth += healingAmount;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        healthBar.value = currentHealth;
    }
}
