﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePortal : MonoBehaviour
{
    [SerializeField] GameObject portal;

    private void OnDestroy()
    {
        Instantiate(portal, gameObject.transform.position, Quaternion.identity);
    }
}
