﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyScript : CharacterScript
{
    [Header("Follow")]
    public float distance;          //дистанция от игрока до врага
    [SerializeField] float radius = 15;       //радиус, при котором враг перестаёт преследовать игрока
    GameObject player;
    NavMeshAgent nav;

    [Header("Die")]
    [SerializeField] GameObject dieEffect;
    [SerializeField] Transform spawnDieEffect;

    SpawnEnemy room;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        nav = GetComponent<NavMeshAgent>();

        healthBar.maxValue = maxHealth;
        healthBar.value = maxHealth;

        room = GetComponentInParent<SpawnEnemy>();
    }

    void FixedUpdate()
    {
        //преследование игрока
        distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance > radius)
        {
            nav.enabled = false;
        }

        else
        {
            nav.enabled = true;
            nav.SetDestination(player.transform.position);
        }
    }

    public override void Die()
    {
        GameObject impact = Instantiate(dieEffect, spawnDieEffect.position, Quaternion.identity);
        Destroy(impact, 1f);
        Destroy(gameObject);
        room.enemies.Remove(gameObject);
    }
}
