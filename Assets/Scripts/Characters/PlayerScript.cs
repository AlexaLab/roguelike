﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : CharacterScript
{
    private int money;
    [SerializeField] Text textMoney;
    [SerializeField] Text textLevel;

    void Start()
    {
        money = PlayerPrefs.GetInt("Money");
        currentHealth = PlayerPrefs.GetFloat("CurrentHealth");
        AddMoney(0);
        Damage(0);

        if (PlayerPrefs.HasKey("Level"))
            textLevel.text = "Уровень " + PlayerPrefs.GetInt("Level");
        else
            textLevel.text = "Уровень 0";
    }

    public void EndLevel()
    {
        PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level") + 1);
        PlayerPrefs.SetInt("Money", money);
        PlayerPrefs.SetFloat("MaxHealth", maxHealth);
        PlayerPrefs.SetFloat("CurrentHealth", currentHealth);
    }

    public override void Die()
    {
        SceneManager.LoadScene("MainMenu");
        PlayerPrefs.SetInt("Level", 1);
        ClearPrefs();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void ClearPrefs()
    {
        PlayerPrefs.SetInt("Money", 0);
        PlayerPrefs.SetFloat("MaxHealth", maxHealth);
        PlayerPrefs.SetFloat("CurrentHealth", maxHealth);
    }

    public void AddMoney(int moneyAmount)
    {
        money += moneyAmount;
        textMoney.text = money + " $";
    }

    public void SubMoney(int moneyAmount)
    {
        money -= moneyAmount;
        textMoney.text = money + " $";
    }
}
