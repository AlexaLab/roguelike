﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] float openingWidth; //Ширина открывания двери
    [SerializeField] float speed;        //скорость открывания двери
    public bool isOpen;                  //открыта ли дверь
    private bool isOpenCloseOn;          //открывается или закрывается ли дверь в данный момент
    public bool isUnlock;                //можно ли открыть дверь

    [SerializeField] GameObject leftLeaf;          //левая и правая створки   
    [SerializeField] GameObject rightLeaf;

    float leftLeafOpen;                  //положение по оси z открытых и закрытых створок
    float rightLeafOpen;
    float leftLeafClose;
    float rightLeafClose;

    [SerializeField] AudioSource sound;  //звук открывания

    private void Start()
    {
        sound = GetComponent<AudioSource>();

        leftLeafClose = leftLeaf.transform.localPosition.z;     //в начале двери закрыты
        rightLeafClose = rightLeaf.transform.localPosition.z; 

        leftLeafOpen = leftLeafClose + openingWidth;
        rightLeafOpen = rightLeafClose - openingWidth;

        isUnlock = true;
    }
    
    public void OpenCloseDoor()
    {
        if (isUnlock)
        {
            sound.Play();
            isOpenCloseOn = true;
            isOpen = !isOpen;
        }
    }

    void StopOpenClose()
    {
        isOpenCloseOn = false;
    }

    private void Update()
    {
        if (isOpenCloseOn)
        {
            if (isOpen)
            {
                float pos = Mathf.MoveTowards(leftLeaf.transform.localPosition.z, leftLeafOpen, speed * Time.deltaTime);
                leftLeaf.transform.localPosition = new Vector3(leftLeaf.transform.localPosition.x, leftLeaf.transform.localPosition.y, pos);

                pos = Mathf.MoveTowards(rightLeaf.transform.localPosition.z, rightLeafOpen, speed * Time.deltaTime);
                rightLeaf.transform.localPosition = new Vector3(rightLeaf.transform.localPosition.x, rightLeaf.transform.localPosition.y, pos);

                if (leftLeaf.transform.position.z == leftLeafOpen) StopOpenClose();
            }
            else
            {
                float pos = Mathf.MoveTowards(leftLeaf.transform.localPosition.z, leftLeafClose, speed * Time.deltaTime);
                leftLeaf.transform.localPosition = new Vector3(leftLeaf.transform.localPosition.x, leftLeaf.transform.localPosition.y, pos);

                pos = Mathf.MoveTowards(rightLeaf.transform.localPosition.z, rightLeafClose, speed * Time.deltaTime);
                rightLeaf.transform.localPosition = new Vector3(rightLeaf.transform.localPosition.x, rightLeaf.transform.localPosition.y, pos);

                if (leftLeaf.transform.position.z == leftLeafClose) StopOpenClose();
            }
        }
    }
}
