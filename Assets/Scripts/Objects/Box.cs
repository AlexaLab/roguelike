﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public bool isOpen;

    [SerializeField] AudioSource openSound;
    [SerializeField] Animation anim;

    void Start()
    {
        openSound = GetComponent<AudioSource>();
    }
    public void Open()
    {
        anim = gameObject.GetComponent<Animation>();
        anim.Play();
        isOpen = true;

        openSound.Play();

        Collider collider = gameObject.GetComponent<Collider>();
        Destroy(collider);
    }
}
