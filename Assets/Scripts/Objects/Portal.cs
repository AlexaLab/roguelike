﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerScript>().EndLevel();

            if (PlayerPrefs.GetInt("Level") > 5)
            {
                SceneManager.LoadSceneAsync("EndGame");
            }
            
            string level = "Level" + PlayerPrefs.GetInt("Level");
            SceneManager.LoadSceneAsync(level);
        }
    }
}
