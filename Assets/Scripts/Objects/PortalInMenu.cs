﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalInMenu : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Cursor.lockState = CursorLockMode.None;   //вернуть курсор
            Cursor.visible = true;
            SceneManager.LoadSceneAsync("MainMenu");
        }
    }
}
