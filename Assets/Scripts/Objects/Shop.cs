﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    [SerializeField] GameObject panelShop;
    [SerializeField] Pause pause;

    public void OpenShop()
    {
        panelShop.SetActive(true);
        pause.panelGame.SetActive(false);
        pause.onOffPlayer(false);

        Time.timeScale = 0f;


        Cursor.lockState = CursorLockMode.None;   //вернуть курсор
        Cursor.visible = true;
    }

    public void Continue()
    {
        panelShop.SetActive(false);
        pause.panelGame.SetActive(true);
        pause.onOffPlayer(true);

        Time.timeScale = 1f;


        Cursor.lockState = CursorLockMode.Locked;  //убрать курсор
        Cursor.visible = false;
    }
}
