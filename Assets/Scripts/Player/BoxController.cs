﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{
    [SerializeField] float distance;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, distance))
            {
                Box box = hit.collider.GetComponent<Box>();

                if ((hit.collider.tag == "BoxMoney" || hit.collider.tag == "BoxMed" ) && box.isOpen == false)
                {
                    box.Open();
                }

                if (hit.collider.tag == "Money")
                {
                    Destroy(hit.collider.gameObject);

                    PlayerScript player = GetComponentInParent<PlayerScript>();
                    int add = Random.Range(5, 20);

                    player.AddMoney(add);
                }

                if (hit.collider.tag == "Med")
                {
                    Destroy(hit.collider.gameObject);

                    PlayerScript player = GetComponentInParent<PlayerScript>();
                    int add = Random.Range(5, 20);

                    player.Healing(add);
                }
            }
        }
    }
}
