﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level0 : MonoBehaviour
{
    [SerializeField] int distance = 5;
    [SerializeField] Text textE;
    [SerializeField] GameObject gun;
    [SerializeField] ShotPlayer shot;

    void Start()
    {
        gun.SetActive(false);
        shot.enabled = false;

        PlayerPrefs.SetInt("Level", 0);
        GetComponentInParent<PlayerScript>().ClearPrefs();
    }

    void Update()
    {
        RaycastHit hit;
        Debug.DrawRay(transform.position, transform.forward * 4, Color.red);
        textE.enabled = false;
        if (Physics.Raycast(transform.position, transform.forward, out hit, distance))
        {
            if (hit.collider.tag == "Door")
            {
                Door door = hit.collider.gameObject.GetComponent<Door>();

                textE.enabled = true;

                if (door.isOpen)
                {
                    textE.text = "Нажмите \"Е\", чтобы закрыть";
                }
                else
                {
                    textE.text = "Нажмите \"Е\", чтобы открыть";
                }
            }

            else if (hit.collider.tag == "BoxMed" || hit.collider.tag == "BoxMoney")
            {
                textE.enabled = true;
                textE.text = "Нажмите \"Е\", чтобы открыть";
            }

            else if (hit.collider.tag == "Med" || hit.collider.tag == "Money")
            {
                textE.enabled = true;
                textE.text = "Нажмите \"Е\", чтобы взять";
            }


            else if (hit.collider.tag == "Gun")
            {
                textE.enabled = true;
                textE.text = "Нажмите \"Е\", чтобы взять";

                if (Input.GetKeyDown(KeyCode.E))
                {
                    gun.SetActive(true);
                    Destroy(hit.collider.gameObject);
                    shot.enabled = true;
                }
            }
        }
    }
}
