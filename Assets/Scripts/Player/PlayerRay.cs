﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRay : MonoBehaviour
{

    [SerializeField] int distance = 5;
    [SerializeField] Text textE;

    void FixedUpdate()
    {
        RaycastHit hit;
        Debug.DrawRay(transform.position, transform.forward * 4, Color.red);
        textE.enabled = false;
        if (Physics.Raycast(transform.position, transform.forward, out hit, distance))
        {
            if (hit.collider.tag == "Door")
            {
                Door door =  hit.collider.gameObject.GetComponent<Door>();

                textE.enabled = true;

                if (door.isOpen)
                {
                    textE.text = "Нажмите \"Е\", чтобы закрыть";
                }
                else
                {
                    textE.text = "Нажмите \"Е\", чтобы открыть";
                }
            }

            else if (hit.collider.tag == "BoxMed" || hit.collider.tag == "BoxMoney")
            {
                textE.enabled = true;
                textE.text = "Нажмите \"Е\", чтобы открыть";
            }

            else if (hit.collider.tag == "Med" || hit.collider.tag == "Money")
            {
                textE.enabled = true;
                textE.text = "Нажмите \"Е\", чтобы взять";
            }

            else if (hit.collider.tag == "Shop")
            {
                textE.enabled = true;
                textE.text = "Нажмите \"Е\", чтобы открыть магазин";
            }
        }
    }
}
