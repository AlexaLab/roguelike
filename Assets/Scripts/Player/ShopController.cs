﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] GameObject shop;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, distance))
            {
                if (hit.collider.tag == "Shop")
                {
                    shop.GetComponent<Shop>().OpenShop();
                }
            }
        }
    }
}
