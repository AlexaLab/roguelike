﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField] Door[] doors;
    [SerializeField] GameObject enemyType;
    [SerializeField] Transform[] enemySpawners;

    [HideInInspector] public List<GameObject> enemies;

    [SerializeField] bool isSpawnEnemies = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isSpawnEnemies)
        {
            isSpawnEnemies = true;

            foreach (Transform spawner in enemySpawners)
            {
                GameObject enemy = Instantiate(enemyType, spawner.position, Quaternion.identity);
                enemy.transform.parent = transform;
                enemies.Add(enemy);
            }

            foreach (Door door in doors)
            {
                if (door.isOpen == true)
                {
                    door.OpenCloseDoor();
                }
                door.isUnlock = false;
            }

            StartCoroutine(CheckEnemies());
        }
    }

    IEnumerator CheckEnemies()
    {
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => enemies.Count == 0);
        OpenDoors();
    }

    void OpenDoors()
    {
        foreach(Door door in doors)
        {
            door.isUnlock = true;
        }
    }
}
