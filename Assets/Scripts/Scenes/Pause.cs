﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    static bool isPause = false;

    public GameObject panelGame;
    [SerializeField] GameObject panelPause;

    [SerializeField] FirstPersonLook firstPersonLook;
    [SerializeField] Zoom zoom;
    [SerializeField] FirstPersonAudio firstPersonAudio;
    [SerializeField] GroundCheck groundCheck;
    [SerializeField] GameObject controller;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;  //убрать курсор
        Cursor.visible = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPause)
            {
                Continue();
            }

            else
            {
                PauseGame();
            }
        }
    }

    public void Continue()
    {
        panelPause.SetActive(false);
        panelGame.SetActive(true);
        onOffPlayer(true);

        Time.timeScale = 1f;

        isPause = false;

        Cursor.lockState = CursorLockMode.Locked;  //убрать курсор
        Cursor.visible = false;
    }

    void PauseGame()
    {
        panelPause.SetActive(true);
        panelGame.SetActive(false);
        onOffPlayer(false);

        Time.timeScale = 0f;

        isPause = true;

        Cursor.lockState = CursorLockMode.None;   //вернуть курсор
        Cursor.visible = true;
    }

    public void onOffPlayer(bool value)
    {
        //отключить всё управление игроком
        GetComponentInParent<FirstPersonMovement>().enabled = value;
        GetComponentInParent<Jump>().enabled = value;
        GetComponentInParent<Crouch>().enabled = value;
        GetComponentInParent<PlayerScript>().enabled = value;
        firstPersonLook.enabled = value;
        zoom.enabled = value;
        firstPersonAudio.enabled = value;
        groundCheck.enabled = value;
        controller.SetActive(value);
    }

    public void ToMainMenu()
    {
        Continue();
        Cursor.lockState = CursorLockMode.None;   //вернуть курсор
        Cursor.visible = true;
        SceneManager.LoadScene("MainMenu");
    }
}
