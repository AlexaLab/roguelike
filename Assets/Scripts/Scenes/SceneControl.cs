﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    public void ToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ToSettings()
    {
        SceneManager.LoadScene("Settings");
    }

    public void ToExit()
    {
        Application.Quit();
    }

    public void ToGame()
    {
        int level;

        if (PlayerPrefs.HasKey("Level")) level = PlayerPrefs.GetInt("Level");
        else level = 0;

        if (level > 5) SceneManager.LoadSceneAsync("EndGame");
        else SceneManager.LoadScene("Level" + level);
    }

    public void ToGameInfo()
    {
        SceneManager.LoadScene("GameInfo");
    }
}
