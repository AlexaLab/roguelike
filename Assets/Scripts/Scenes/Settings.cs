﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
    [SerializeField] Dropdown dropdownResoiution;
    [SerializeField] Dropdown dropdownQuality;

    Resolution[] resolutions;

    void Start()
    {
        dropdownResoiution.ClearOptions();
        List<string> options = new List<string>();
        resolutions = Screen.resolutions;
        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height + " " + resolutions[i].refreshRate + "Гц";
            options.Add(option);

            if (resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
                currentResolutionIndex = i;
        }

        dropdownResoiution.AddOptions(options);
        dropdownResoiution.RefreshShownValue();
        LoadSettings(currentResolutionIndex);
    }

    public void SetFullScreen(bool isFull)
    {
        Screen.fullScreen = isFull;
    }

    public void SetResolution(int index)
    {
        Resolution resolution = resolutions[index];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetQuality(int index)
    {
        QualitySettings.SetQualityLevel(index);
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetInt("Quality", dropdownQuality.value);
        PlayerPrefs.SetInt("Resolution", dropdownResoiution.value);
        PlayerPrefs.SetInt("FullScreen", Convert.ToInt32(Screen.fullScreen));
    }

    public void LoadSettings(int currentResolutionIndex)
    {
        if (PlayerPrefs.HasKey("Quality"))
            dropdownQuality.value = PlayerPrefs.GetInt("Quality");
        else
            dropdownQuality.value = 3;

        if (PlayerPrefs.HasKey("Resolution"))
            dropdownResoiution.value = PlayerPrefs.GetInt("Resolution");
        else
            dropdownResoiution.value = currentResolutionIndex;

        if (PlayerPrefs.HasKey("FullScreen"))
            Screen.fullScreen = Convert.ToBoolean(PlayerPrefs.GetInt("FullScreen"));
        else
            Screen.fullScreen = true;
    }
}
