﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BangBangEnemy : MonoBehaviour
{
    [Header("Bang")]
    [SerializeField] float damage;        //урон от взрыва
    [SerializeField] Slider bangBar;      //слайдер с временем до взрыва
    [SerializeField] float bangTime;      //время до взрыва

    [Header("Effects")]
    [SerializeField] AudioSource bangSound;  //звук взрыва
    [SerializeField] GameObject bangEffect;  //взрыв
    [SerializeField] Transform bangSpawn;    //место взрыва

    EnemyScript enemy;

    void Start()
    {
        bangBar.maxValue = bangTime;
        bangBar.value = bangTime;

        enemy = GetComponent<EnemyScript>();
    }

    private void Update()
    {
        if (enemy.distance <= 3)
        {
            if (bangTime >= 0)
            {
                bangTime -= Time.deltaTime;
                bangBar.value = bangTime;
            }

            else
            {
                PlayerScript player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
                player.Damage((1 / enemy.distance) * damage);   //урон зависит от дистанции до взрыва

                GameObject impact = Instantiate(bangEffect, bangSpawn.position, Quaternion.identity);
                Destroy(impact, 2f);
                enemy.Die();


            }
        }
    }
}
