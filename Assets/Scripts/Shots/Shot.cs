﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [SerializeField] protected float damage = 25f;     //урон от выстрела
    [SerializeField] protected float fireRate = 0.25f; //время между выстрелами
    [SerializeField] protected float distance = 15;    //дальность выстрела
    protected float nextFire;                          //игровое время

    [SerializeField] protected Transform spawnLaser;   //точка спавна лазера
    protected WaitForSeconds laserDuration = new WaitForSeconds(0.05f);  //время отрисовки лазера
    protected LineRenderer laserLine;                  //лазер

    [SerializeField] protected AudioSource shotSound;  //звук выстрела

    protected IEnumerator ShotEffect()
    {
        shotSound.Play();
        laserLine.enabled = true;
        yield return laserDuration;
        laserLine.enabled = false;
    }
}
