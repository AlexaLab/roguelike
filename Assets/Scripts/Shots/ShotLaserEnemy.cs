﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotLaserEnemy : Shot
{
    [SerializeField] AudioSource shotHit;  //звук попадания по игроку

    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        nextFire = Time.time + 3;
    }

    void Update()
    {
        if (Time.time > nextFire)
        {
            fireRate = Random.Range(2, 4);
            nextFire = Time.time + fireRate;
            StartCoroutine(ShotEffect());

            RaycastHit hit;
            laserLine.SetPosition(0, spawnLaser.transform.position);

            if (Physics.Raycast(spawnLaser.transform.position, spawnLaser.transform.forward, out hit, distance))
            {
                laserLine.SetPosition(1, hit.point);
                PlayerScript player = hit.collider.GetComponent<PlayerScript>();

                if (player != null)
                {
                    player.Damage(damage);
                    shotHit.Play();
                }
            }

            else
            {
                laserLine.SetPosition(1, gameObject.transform.forward * distance);
            }
        }
    }
}
