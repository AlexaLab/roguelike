﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotPlayer : Shot
{
    [SerializeField] Transform spawnRay;

    [SerializeField] GameObject hitEffect;         //эффект после выстрела

    void Start()
    {
        shotSound = GetComponent<AudioSource>();
        laserLine = GetComponent<LineRenderer>();
        nextFire = 0;
    }

    void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(ShotEffect());

            RaycastHit hit;
            laserLine.SetPosition(0, spawnLaser.position);

            if (Physics.Raycast(spawnRay.position, spawnRay.forward, out hit, distance))
            {
                laserLine.SetPosition(1, hit.point);
                EnemyScript enemy = hit.collider.GetComponent<EnemyScript>();

                if (enemy != null)
                {
                    enemy.Damage(damage);

                    GameObject impact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
                    Destroy(impact, 1f);
                }
            }

            else
            {
                laserLine.SetPosition(1, spawnRay.forward * distance);
            }
        }
    }
}
